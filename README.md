## Overview ##

Webview addon for Codebox, which displays graphical simulator of a board in an
HTML iframe.

## Install ##

To install addon, you must have codebox setup with core addons installed

    git clone git@bitbucket.org:Code911/codebox.git && cd codebox
    npm install
    npm install -g gulp jsctags
    export NODE_PATH=$NODE_PATH/jsctags/jsctags:$NODE_PATH
    gulp
    mkdir -p ~/.codebox/packages
    node ./bin/codebox.js install

In the command below, replace `<bitbucket user>` with actual bitbucket login.

        node ./bin/codebox.js install -p 'webview:https://<bitbucket user>@bitbucket.org/Code911/package-webview.git'

Finally, run codebox. Specifiy folder for workspace root.
The plugin can detect automatically relative or absolute url. There are two methods to configure the URL to display

         WEBVIEW_URL='http://www.myabsoluteurl.com'; node ./bin/codebox.js run /some/folder 
         or        
         node ./bin/codebox.js run /some/folder --settings='{ "webview": ":8080/myrelativeurl" }'

## Addon setup ##

Before launching codebox, there should be webview app running on
localhost:8080. This address in not configurable. It is possible to
setup a proxy server that will redirect all requests from localhost:8080 to
proper address.