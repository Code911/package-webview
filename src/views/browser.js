var View = codebox.require("hr.view"),
    $ = codebox.require("jquery");

var BrowserView = View.extend({
    className: "browser",
    
    initialize: function() {
        codebox.settings.settings.webview
        var pathArray = window.location.host.split( ':' );
        var src = undefined;
        var url = codebox.settings.settings.webview.URL;
        var r = new RegExp('^(?:[a-z]+:)?//', 'i');
        if (r.test(url)) {
            // absolute path
            src = url;
        }
        else {
             src = "//" + pathArray[0] + url;

        }
        BrowserView.__super__.initialize.apply(this, arguments);
        $("<iframe>", {
            "class": "content",
            "src": src,
            "sandbox": "allow-forms allow-same-origin allow-scripts"
        }).appendTo(this.$el);
        return this;
    }
});

module.exports = BrowserView;
