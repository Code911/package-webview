require("./stylesheets/main.less");

var Browser = require("./views/browser");
var commands = codebox.require("core/commands");
var settings = require("./settings");
var _ = codebox.require("hr.utils");
var rpc = codebox.require("core/rpc");
var dialogs = codebox.require("utils/dialogs");

codebox.app.once("ready", function() {

   var temp = codebox.settings.exportJson();
   var obj = [{"command": "webview.open"}];             
   _.extend(temp.tree.toolbar, obj);
   codebox.settings.importJSON(temp);


    rpc.execute('webview/config')
        .then(function(msg) {
            var temp = codebox.settings.exportJson();
            temp.webview.URL = msg;
            codebox.settings.importJSON(temp);
            commands.run("webview.open");            
            return;
    });
}),


commands.register({
    id: "webview.open",
    title: "Webview: Open",
    icon: "globe",
    shortcuts: [
        "alt+w"
    ],
    run: function() {
        codebox.tabs.add(Browser, {}, {
            title: "Webview",
            icon: "globe"
        });
    }
});




