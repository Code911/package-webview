module.exports = codebox.settings.schema("webview",
    {
        "title": "Webview",
        "type": "object",
        "properties": {
            "URL": {
                "description": "Show URL in webview",
                "type": "string",
                "default": ":8080/"
            }
        }
    }
);