module.exports = function(codebox) {
    var workspace = codebox.workspace;
    var events = codebox.events;

    codebox.rpc.service("webview", {
        config: function(args) {
            return workspace.config("webview") || process.env.WEBVIEW_URL;
        }
    });
};